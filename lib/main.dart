import 'package:blocwithchopper/features/blocwithchopper/data/repositories/weather_repositories_impl.dart';
import 'package:blocwithchopper/features/blocwithchopper/domain/rest/weather_service.dart';
import 'package:blocwithchopper/features/blocwithchopper/presentation/bloc/bloc.dart';
import 'package:blocwithchopper/features/blocwithchopper/presentation/screens/home_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bloc With Chopper',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (context) => WeatherBloc(
            WeatherRepositoryImpl(mApiService: WeatherApiService.create())),
        child: MyHomePage(
          title: "Weather App",
        ),
      ),
    );
  }
}
