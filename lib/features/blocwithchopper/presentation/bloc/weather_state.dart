import 'package:blocwithchopper/features/blocwithchopper/data/models/weather_response.dart';
import 'package:equatable/equatable.dart';

abstract class WeatherState extends Equatable {
  const WeatherState();
}

class WeatherInital extends WeatherState {
  const WeatherInital();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class WeatherLoading extends WeatherState {
  const WeatherLoading();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class WeatherLoaded extends WeatherState {
  final WeatherResponse weatherResponse;

  const WeatherLoaded(this.weatherResponse);

  @override
  // TODO: implement props
  List<Object> get props => [weatherResponse];
}
