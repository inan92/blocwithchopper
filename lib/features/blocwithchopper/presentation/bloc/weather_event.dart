import 'package:equatable/equatable.dart';

abstract class WeatherEvent extends Equatable {
  const WeatherEvent();
}

class GetWeather extends WeatherEvent {
  final String cityName;
  final String appid;

  const GetWeather(this.cityName, this.appid);

  @override
  // TODO: implement props
  List<Object> get props => [cityName, this.appid];
}
