import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:blocwithchopper/features/blocwithchopper/data/models/weather_response.dart';
import 'package:blocwithchopper/features/blocwithchopper/domain/repositories/weather_repositories.dart';

import './bloc.dart';

class WeatherBloc extends Bloc<WeatherEvent, WeatherState> {
  final WeatherRepository mWeatherRepository;

  WeatherBloc(this.mWeatherRepository);

  @override
  WeatherState get initialState => WeatherInital();

  @override
  Stream<WeatherState> mapEventToState(
    WeatherEvent event,
  ) async* {
    // TODO: Add Logic
    yield WeatherLoading();
    if (event is GetWeather) {
      final weatherResponse = await mWeatherRepository.getWeatherResponse(
          event.cityName, event.appid);

      print(weatherResponse.bodyString);
      try {
        final Map<String, dynamic> map =
            json.decode(weatherResponse.bodyString);
        WeatherResponse mResponse = WeatherResponse.fromJson(map);
        yield WeatherLoaded(mResponse);
      } catch (e) {
        print(e);
      }
    }
  }
}
