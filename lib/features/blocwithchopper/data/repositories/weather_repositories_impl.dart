import 'package:blocwithchopper/features/blocwithchopper/data/models/weather_response.dart';
import 'package:blocwithchopper/features/blocwithchopper/domain/repositories/weather_repositories.dart';
import 'package:blocwithchopper/features/blocwithchopper/domain/rest/weather_service.dart';
import 'package:chopper/chopper.dart';
import 'package:meta/meta.dart';

class WeatherRepositoryImpl implements WeatherRepository {
  final WeatherApiService mApiService;

  WeatherRepositoryImpl({@required this.mApiService});

  @override
  Future<Response> getWeatherResponse(String city, String appid) {
    // TODO: implement getWeatherResponse
    return mApiService.getWeatherResponse(city, appid);
  }
}
