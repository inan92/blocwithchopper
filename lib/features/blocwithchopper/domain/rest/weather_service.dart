import 'package:blocwithchopper/core/util/constants.dart';
import 'package:blocwithchopper/features/blocwithchopper/data/models/weather_response.dart';
import 'package:chopper/chopper.dart';
part 'weather_service.chopper.dart';

@ChopperApi(baseUrl: '/weather')
abstract class WeatherApiService extends ChopperService {
  static WeatherApiService create() {
    final client = ChopperClient(
        baseUrl: BASE_URL,
        services: [
          _$WeatherApiService()
        ],
        converter: JsonConverter()
    );    return _$WeatherApiService(client);
  }

  @Get()
  Future<Response> getWeatherResponse(
      @Query('q') String city, @Query('appid') String appId);
}
