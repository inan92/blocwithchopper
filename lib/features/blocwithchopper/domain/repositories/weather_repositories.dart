import 'package:chopper/chopper.dart';

abstract class WeatherRepository {
  Future<Response> getWeatherResponse(String city, String appid);
}
